#+TITLE: CBSH SDDM Theme
#+AUTHOR: Cameron Miller (Cabooshyy)
#+DESCRIPTION: A custom SDDM Theme

* TABLE OF CONTENTS :toc:
-   [[#cbsh-sddm-theme][CBSH SDDM Theme]]
- [[#dependencies][Dependencies]]
- [[#installing-the-theme][Installing the theme]]
- [[#licence][LICENCE]]

*   CBSH SDDM Theme
This is the custom SDDM theme based off of [[https://github.com/MalditoBarbudo/solarized_sddm_theme][solarized_sddm_theme]] with some colour changes to match my overall theme for the desktop. Since SDDM themes are completely new to me doing it this way lets me learn a bit more about how QML works and such.

Since this will contain the default theme, as i learn more about creating themes and create my own theme i will update this repo (which will consequently update the arch repo).

[[https://gitlab.com/cbsh/cbsh-sddm-theme/-/raw/main/cbsh-sddm-preview.png]]

* Dependencies
 - [[https://archlinux.org/packages/community/any/ttf-font-awesome/][ttf-font-awesome]] -- The Base Theme relies on the FontAwesome Fonts being installed to work correctly, the link will take you to the arch package, on other systems check your package manager before manually installing them.

* Installing the theme
 - Arch Linux and Arch Based

The Theme has a PKGBUILD that you can use to install a local version of this theme, if you have my CBSH repo enabled in your pacman.conf then you can install it using

=$ sudo pacman -S cbsh-sddm-theme=

- Other Linux Distributions
=This theme is only tested on Arch Linux as of right now, it should work on other Distributions, but use it at your own risk.=

To Install this theme on other distros, first off clone this repo:

#+begin_src bash
git clone https://gitlab.com/Cabooshyy/cbsh-sddm-theme.git
#+end_src

Then copy the contents of the cloned repo into =/usr/share/sddm/themes/cbsh-sddm-theme=

Finally Change the =/usr/lib/sddm/sddm.conf.d/default.conf= to use the new theme.


* LICENCE
The Theme is Licenced under GPL

QML Files (QT Markup Language) are licenced under MIT
